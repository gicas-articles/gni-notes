\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[english,russian]{babel}
\usepackage{amssymb,amsfonts,amsmath}
\usepackage{cmap}
\usepackage{listings}
\usepackage{hyperref}
\usepackage[backend=biber]{biblatex}
\addbibresource{krylov-lanczos.bib}

\newtheorem{theorem}{Theorem}

\newcommand{\norm}[1]{\left\lVert#1\right\rVert_2}
\newcommand{\normfro}[1]{\left\lVert#1\right\rVert_F}

\begin{document}
	
	$ \rho=\norm{A} $, $ \beta = \norm{v} $, $ \lambda_1<...< \lambda_n $ --- eigenvalues of $ A $.

	$ J_k(x) $ --- Bessel functions of complex argument.

	$ T_k(x) $ --- Chebyshev polynomials of 1st kind.

	
	\section{Krylov subspace method with Arnoldi iteration}
	$ A $ is non-symmetric.
	\subsection{Arnoldi process}
	Fixed algorithm from \cite{Saad1989}
	\begin{lstlisting}[mathescape]
	for k=1:m 
	  $ v_{k+1} = A v_k $ 
	  for j=1:k 
	    $ H_{j,k} $=$ <v_j,v_{k+1}> $ 
	    $ v_{k+1}=v_{k+1}-H_{j,k}v_{j} $ 
	  endfor 
	endfor 
	if k<m 
	  $ H_{k+1,k}=\norm{V_{k+1}}  $
	  $ V_{k+1}=v_{k+1}/H_{k+1,k} $
	endif
	\end{lstlisting}
	\begin{equation}
	u=f(A)v, \:
	u_m=\beta Qf(H_m)e_1
	\end{equation}
	\subsection{Error estimation}
	\begin{theorem}\cite{Saad1989}
	\begin{equation}
		\norm{u-u_m}\leq 2 \beta \frac{\rho^m e^\rho}{m!}.
	\end{equation}
	\end{theorem}
	\begin{theorem}\cite{Knizhnerman1991a}
		For some conditions posed upon the matrices during the Arnoldi iteration:
		 \begin{align}
		 \norm{u-u_m}&\lesssim
		 \max_{z\in\Gamma}|f(z)| \beta \norm{F} m^{2\alpha+2,5} 
		 \max_{0\leq l\leq m-1}(1+1/\bar{l}+\eta^{1/\alpha})^t+\\ 
		 &+ \beta\sum_{k=m}^{\infty}|f_k|(1+1/k+\eta^{1/\alpha})^k k^\alpha
		 \end{align}
		 where $ \alpha \geq 1 $ and the constant left to $ \lesssim $ depend on $ cl(K) $, $ K $ is the set Rayleigh quotients of $ A $.
	\end{theorem}
	\begin{theorem}\cite{Saad1992}
		Following equation is asymptotically optimal in case when eigenvalues are distributed in a cicle centered at the origin:
		\begin{equation}\label{key}
		\norm{error}\leq 2*\beta \frac{|\lambda_n|^m}{m!}(1+o(1))
		\end{equation}
	\end{theorem}
	\begin{theorem}\cite{Saad1992}
		$ \forall A $, $ \rho_\alpha =\norm{A-\alpha I}, \forall \alpha\in\mathbb{R}$:
		\begin{equation}\label{key}
		\norm{e^Av-u_m} \leq 2\beta e^\alpha t_m(\rho_\alpha)\leq 2\beta\frac{\rho_{\alpha}^{m}\exp(\rho_\alpha+\alpha)}{m!}
		\end{equation}
		For corrected scheme:
		\begin{equation}\label{key}
		\norm{e^Av-\bar{u}_m}\leq 2\beta\frac{\rho_0^{m+1}e^{\rho_0}}{(m+1)!}
		\end{equation}
	\end{theorem}
	
	\begin{theorem}\cite{Saad1992}
		\begin{equation}
			Er_5 = h_{m+1,m}|e^T_m \phi_2(H_m)\beta e_1|\normfro{H_m}
		\end{equation}
		\begin{equation}
		\phi_0(z)=e^z, \: \phi_{k+1}=\frac{\phi_k(z)-\phi_k(0)}{z}, k \geq 0.
		\end{equation}
	\end{theorem}
	\begin{theorem}\cite{Hochbruck1996}
		Stopping criterion overly optimistic if $ \norm{\tau A} $ is large, where $ A $ is Jacobian of he system, $ \phi=(e^z-1)/z $:
		\begin{equation}
			h \beta \tau h_{m+1,m} \left|\left[(I-\tau H_m)^{-1} \phi(\tau H_m)\right]_{m,1} \right| \cdot \norm{v_{m+1}} \leq 0.1
		\end{equation}
	\end{theorem}
	
	\section{Lanczos method}
	$ H $ is tridiagonal (Hessenberg?)), $ Q $ is a basis matrix.
	\cite{Druskin1989}
	\begin{equation}
	v_m=Qf(H)e_1
	\end{equation}
	\begin{equation}
	V=\frac{\lambda_n+\lambda_1}{\lambda_n-\lambda_1}E_n-\frac{2}{\lambda_n-\lambda_1}H
	\end{equation}
	\subsection{Error estimation}
	\begin{theorem}\cite{Druskin1989}
	\begin{equation}
		\begin{split}
		u-v_m=\sum_{k=m}^{\infty}g_k(T_k(B)v-QT_k(V)e_1)\\
		\norm{u-v_m}=2\sum_{k=m}^{\infty}|g_k|<+\infty.
		\end{split}
	\end{equation}
	\end{theorem}
	\begin{theorem}\cite{Druskin1989}
		For a parabolic equation, when $ m\leq a $, $ a=t\lambda_n/2 $, $ B=E_n-2A/\lambda_n $, error bound both for Lanczos and Chebyshev:
	\begin{equation}
		\begin{split}
			\exp(ax)=\sum_{k=0}^{\infty}J_k(a)T_k(x),\\
			u=\exp(-a)\sum_{k=0}^{\infty}J_k(a)T_k(B)v.\\
			\norm{u-u_m}=[\sqrt{2\pi}+O(a^{-1})]\frac{\sqrt{a}}{m}\exp[-\frac{m^2}{2a}+O(\frac{m^4}{a^3})].
		\end{split}
	\end{equation}
	\begin{theorem}\cite{Saad1992}
		If $ A $ is symmetcric definite negative and $ \rho=\rho $:
		\begin{equation}\label{key}
		\norm{e^A v - u_m}\leq \beta \frac{\rho^m}{m! 2^{m-1}}
		\end{equation}
	\end{theorem}
	\end{theorem}
	\begin{theorem}\cite{Stewart1996}
		$ A $ is symmetric, $ \forall v $.
	\begin{multline}\label{key}
		\norm{e^{-A}v - Q_m e^{-H_m} Q^{T}_{m} v} \leq 4 \beta e^{-\lambda_{min}} \left(\exp(-bm^2/\rho)\left[1+\sqrt{\frac{\rho \pi}{4b}}  \right]+\frac{d^\rho}{1-d} \right),\\ \: b=2/(1+\sqrt{5}), \: d=\frac{e^{2/(1+\sqrt{5})}}{2+\sqrt{5}}
	\end{multline}
	\end{theorem}
	\begin{theorem}\cite{Hochbruck1997}
		A is Hermitian negative semi-definite with eigenvalues in $ \left[-4\rho, 0 \right] $ then Arnoldi error is:
		\begin{equation}\label{key}
		\norm{e^{\tau A}v-Vm e^{\tau H_m}e_1}\leq 10 \exp \left(\frac{-m^2}{5\rho\tau}\right),\sqrt{4\rho\tau}\leq m \leq 2\rho\tau
		\end{equation}
		\begin{equation}\label{key}
		err \leq 10 (\rho\tau)^{-1} exp(-\rho\tau) \left(\frac{e\rho\tau}{m}\right)^m, m\geq 2\rho\tau
		\end{equation}
	\end{theorem}
	
	
	\section{Chebyshev method}
	\begin{equation}
	u=f(A)\phi, \lambda_n>\lambda_1., \norm{\phi}=1.
	\end{equation}
	\begin{equation}\label{key}
	u_m=p_m(A)\phi, \deg p_m <m
	\end{equation}
	\begin{equation}
	B=\frac{\lambda_n+\lambda_1}{\lambda_n-\lambda_1}E_n-\frac{2}{\lambda_n-\lambda_1}A
	\end{equation}
	\begin{equation}
	g(x)=f(\frac{\lambda_n+ \lambda_1-(\lambda_n-\lambda_1)x}{2})
	\end{equation}
	\begin{equation}\label{key}
	g_k=\frac{min(2,k+1)}{\pi}\int_{-1}^{1}g(x)T_k(x)(1-x^2)^{-1/2}dx
	\end{equation}
	\subsection{Error estimation}
	\cite{Druskin1989}
	\begin{equation}
	\norm{u-u_m}=\sum_{k=m}^{\infty}|g_k|<+\infty 
	\end{equation}
	
	
	\printbibliography
\end{document}
